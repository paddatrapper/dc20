# Available in buster-backports
Django>=2.0,<3.0

# Available in Debian
PyYAML
django-countries
psycopg2
python-memcached

# From the cheeseshop
django-crispy-forms>=1.7.0
mdx_linkify==1.2
stripe==2.48.0
wafer==0.9.2
wafer-debconf==0.3.10

# unused but a dependency of django-bakery
# pinned to avoid having to update it every day
boto3==1.9.119
botocore==1.12.119
