import datetime
import logging
import os

from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.http import Http404
from django.urls import reverse, reverse_lazy
from django.utils.feedgenerator import Atom1Feed
from django.utils.timezone import make_aware
from django.utils.translation import ugettext_lazy as _

from bakery.feeds import BuildableFeed
from bakery.views import BuildableTemplateView
from markitup.fields import render_func

from news.models import NewsItem

log = logging.getLogger(__name__)


class NewsItemView(BuildableTemplateView):
    template_name = 'news/item.html'

    def get_context_data(self, date, slug):
        context = super(NewsItemView, self).get_context_data()
        try:
            context['object'] = NewsItem.objects.get_by_url(date, slug)
        except NewsItem.DoesNotExist:
            raise Http404
        return context

    @property
    def build_method(self):
        return self.build_news_items

    def build_news_items(self):
        [self.build_news_item(item) for item in NewsItem.objects.all()]

    def build_news_item(self, item):
        log.debug("Building %s", item)
        path = item.get_absolute_url().lstrip('/') + 'index.html'
        self.request = self.create_request(item.get_absolute_url())
        self.kwargs = {
            'date': item.date,
            'slug': item.slug,
        }
        content = self.get(self.request, **self.kwargs).render().content
        self.prep_directory(path)
        target_path = os.path.join(settings.BUILD_DIR, path)
        self.build_file(target_path, content)


class NewsFeedView(BuildableTemplateView):
    template_name = 'news/feed.html'
    build_path = '/news/index.html'

    def get_context_data(self, page=0):
        context = super(NewsFeedView, self).get_context_data()
        context['stories'] = NewsItem.objects.all()
        return context


class NewsRSSView(BuildableFeed):
    title = _('News')
    link = reverse_lazy('news')
    description = _('Conference News')

    def get_feed(self, obj, request):
        site = get_current_site(request)
        self.title = site.name
        self.description = _('News from %s') % site.name
        return super(NewsRSSView, self).get_feed(obj, request)

    def items(self):
        return NewsItem.objects.all()

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return render_func(item.body)

    def item_link(self, item):
        return item.get_absolute_url()

    def item_pubdate(self, item):
        dt = datetime.datetime.combine(item.date, datetime.time())
        return make_aware(dt)

    def item_updateddate(self, item):
        return self.item_pubdate(item)

    @property
    def build_path(self):
        return reverse('news_rss').lstrip('/')


class NewsAtomView(NewsRSSView):
    feed_type = Atom1Feed
    subtitle = NewsRSSView.description

    def item_author_name(self, item):
        return self.title

    @property
    def build_path(self):
        return reverse('news_atom').lstrip('/')
