---
title: Registration for DebConf20 Online is Open
---

The DebConf team is glad to announce that registrations for DebConf20 Online is
now open. To register for DebConf20, please visit our website at

https://debconf20.debconf.org/register

Participation to DebConf20 is conditional to your respect of our Code of
Conduct [0]. We require you to read, understand and abide by this code.

[0]: <https://debconf.org/codeofconduct.shtml>

A few notes about the registration process:

- We need to know attendees' locations to better plan the schedule around
  timezones. Please make sure you fill in the "Country I call home" field in
  the registration form accordingly. It's specially important to have this
  data for people who submitted talks, but also for other attendees.

- There is an attempt to provide DebConf20 T-shirts, so there is a field for
  that as well. As you might imagine, the logistics of that is not exactly
  easy, so there are no guarantees that it actually happens.

- We are also offering financial support for those who require it in order to
  attend due to the current economic crisis. Please refer to the
  [corresponding page](/about/bursaries/) at the website for more information.

Any questions about registrations should be addressed to
<registration@debconf.org>.
